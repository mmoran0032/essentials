# High-functioning remote teams

*Adapted from [Twitter thread][1].*

## Full content

Last fall I joined a fully remote engineering team that spans several time zones and has been operating remotely for its whole existence. It does remote very well! We like each other and we get work done! Some observations: a thread

We almost never DM! We have a team channel that’s just the 9 of us and we use it to talk about work and also socialize a bit—like a mission-focused group chat. Any observations or questions get put to the group, reducing knowledge silos

We have STRONG boundaries around work hours. You’re free to work or not work on whatever schedule works for you (I’m sometimes on at 10pm) but if someone says they’re off for the day they’re off, and unless someone says otherwise, the day is assumed to end at their 5pm

This goes back to our written communication being very important. Because our communication is asynchronous, written records matter a lot. This also means meetings and appointments must be planned out in advance so schedules are respected and time zones are taken into account

Like DMs, meetings that only two people know about are not encouraged. If two people set an impromptu meeting to discuss a feature or bug fix, they post about it in main chat so others can join to learn or offer advice. Attendance is optional though—we want to avoid zoom fatigue

Our manager leads by example by drawing boundaries on his time and by letting us know when he’s taking time to go for a bike ride or something—exercise is important to our team! He works a lot and gets a lot done, but he has a life and makes it clear that so should we

Our meetings have a bit of optional time at the end to socialize and catch up. We’ll also occasionally have a “coffee hour” in someone’s meeting room just to chat. Like all meetings that aren’t on the calendar, these are optional. But they’re nice

My key takeaways from our team are:
1) respect work/life boundaries 
2) write everything down
3) instead of DMs, invite everyone to discussions, no matter how small
4) make room for the socialization time that would normally happen in an office—but make it optional (boundaries!)

[1]: https://mobile.twitter.com/buttpraxis/status/1399057827063238657
