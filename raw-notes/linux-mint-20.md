
# Linux Mint 20 Installation

## General

- create Terminal system shortcut:
  - System > Keyboard > Shortcuts > Launchers
- update "launch browser" shortcut to Fn + F4 (?)
- change Calendar date format to `%Y-%m-%e %H:%M:%S`
- generate new SSH key
  - same steps as before
  - add SSH key to GitHub and GitLab
- install: git build-essentials tree xclip
- change git location for apt: https://idroot.us/install-git-linux-mint-19/
  - should be done before installing git
  - add-apt-repository ppa:git-core/ppa
  - apt update
  - apt install git
- clone essentials from gitlab
- install VSCode (not that Codium would be better, but the extensions I use still send telemetry data to MSFT...)
- install `zsh` and Oh My ZSH:
  - `sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"`
  - reinitialize conda to show env: `conda init zsh`
  - make ZSH default: `chsh -s $(which zsh)` (requires restart to take effect)
- download Timeular from website: https://timeular.com/download/
  - move to `bin`
  - make executable, move to `/opt`, create symlink to `/usr/local/bin`
  - add menu item:
    - Configure Menu, "New Item" in Accessories
    - use the public API page to grab the icon: https://developers.timeular.com/

## Python

- install `miniconda`: https://docs.conda.io/en/latest/miniconda.html
- install mamba: `conda install mamba -c conda-forge`
- use mamba for everything else
  - black isort mypy pytest

## Others

- install Steam through the website
- update your system settings
