# General tweaks
alias date="date --iso-8601=seconds"
alias df="df -H"
alias ps="ps -Ac"
alias rm="rm -i"
alias open="xdg-open"

# Programming Help
alias jl="jupyter lab"
alias isort="isort --profile black --fss"
alias vim="nvim"

