vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.number = true
vim.opt.termguicolors = true

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  {"nvim-tree/nvim-web-devicons"},
  {"nvim-lualine/lualine.nvim", dependencies = { "nvim-tree/nvim-web-devicons" }},
  {"marko-cerovac/material.nvim"},
  {"nvim-tree/nvim-tree.lua"}
})

vim.cmd("colorscheme material")

require("nvim-web-devicons").setup()
require("lualine").setup({
  options = {
    icons_enabled = false,
    theme = "material-stealth"
  }
})
require("material").setup({
  contrast = {
    terminal = true,
    sidebars = true,
    cursor_line = true
  },
  lualine_style = "stealth"
})
require("material.functions").change_style("deep ocean")
require("nvim-tree").setup()
