
# Docker Installation

Docker and containerization is an important part of modern software
development, and data science is no different. Getting Docker up and
running is important both for your personal development as well as
running pre-built images for useful tools or environments.

The steps here are the high-level steps from the setup guides for
[Docker][1] and [Docker Compose][2].

1. Set up the stable repository to pull from. Note that since Linux Mint
   uses Ubuntu on the backing, you need to use the Ubuntu distribution
   name. You can find it by running `apt update` and watching for the
   name in the output. Currently, I am on Linux Mint `tricia` which
   corresponds to Ubuntu `bionic`.

1. Ensure your installation works by running (from documentation)
   `sudo docker run hello-world`

1. Update and install the Docker engine. You will need to add your user
   to the `docker` group with

   ```sh
   sudo usermod -aG docker $(whoami)
   ```

   Before this can take effect, you'll need to log out and back in, not
   just start a new Terminal session.

1. Download and install the current stable release of `docker-compose`.

At this point, you should be set. Note that `zsh` will already have
Docker-related plugins available to ease working with containers.

[1]: https://docs.docker.com/engine/install/ubuntu/
[2]: https://docs.docker.com/compose/install/
