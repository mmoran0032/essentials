
# TEXT EDITORS

You switch between multiple editors a lot, so this will help you get
them set up. Right now, you are using `vim` for small edits to single
files and `code` for larger projects. Both are essential tools for your
work.

## VSCode

This is your main editor. You've moved this information to your website
and updated it for your current install workflow.

### Windows

With the Windows Subsystem for Linux (WSL), I can use the Linux side of
my machine with the usual Windows interface. After setting up and
installing WSL (see `WINDOWS.md`), you are more or less good to go with
regular Linux installs for coding environments. There are some
additional tweaks to get VSCode working well with WSL:

- Install the *Remote Development extension pack* (see
  [VSCode's website][1] for additional details). Note that the contained
  packages don't appear to be available for install with Codium.
- (Optionally) link directories across the Windows-Linux divide to allow
  for them to be viewable in both places
- Manually copy over the `settings.json` file. Since that file location
  is within the Windows side, the setup script will not push it to the
  correct location.

## Vim

Vim is useful for short edits to files and for longer `git` commit
messages, but past that you usually just use VSCode. The configuration
details include your `.vimrc` file and the `.vim` directory that
contains the additional packages you'll need. All of these are linked
through the basic setup instructions, so there's nothing else to do.

As a note, this has been probably your longest lasting editor
configuration, since you first encountered `vim` early in your graduate
career.

## Obsidian

Obsidian is a personal knowledge graph backed by Markdown files. I am
planning on using it to track learning and development, as well as for
work-related systems.

- [automatic git sync][2]
- download information
- anything else?

[1]: https://code.visualstudio.com/docs/remote/wsl
[2]: https://forum.obsidian.md/t/obsidian-github-integration-for-sync-and-version-control/6369
