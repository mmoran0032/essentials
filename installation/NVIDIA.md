# NVIDIA Installation

For any GPU-accelerated task, you need to set up your GPU to actually
talk to the systems you need. Currently, your desktop GPU is
**NVIDIA GeForce GTX 1060 6GB (GTX 10 Series)**

You can get the current one with `sudo lshw -numeric -C display`. To
use the GPU in any learning task, you need to install the CUDA toolkit
following the [installation guide][2].

Install CUDA 10.1.2 following (see [nvidia][1]):

```sh
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/ /"
sudo apt-get update
sudo apt-get -y install cuda-10-1
```

After these steps, the command `which nvcc` should give you:

```bash
$ nvcc --version
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2019 NVIDIA Corporation
Built on Sun_Jul_28_19:07:16_PDT_2019
Cuda compilation tools, release 10.1, V10.1.243
```

Other versions of the CUDA toolkit can be installed if necessary. To
update the version used, you'll want to update the symbolic link:

```bash
rm -f /usr/local/cuda
ln -s /usr/local/cuda-X.Y /usr/local/cuda
```

[1]: https://developer.nvidia.com/cuda-10.1-download-archive-update2?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1804&target_type=debnetwork
[2]: https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html
