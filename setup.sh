#!/bin/zsh
# set up local device to a decent default terminal state

echo "[+] base path: $(pwd)"
echo "[+] creating directory structure"

mkdir -p "${HOME}/.config"
mkdir -p "${HOME}/.config/1Password/ssh"
mkdir -p "${HOME}/.config/Code/User"
mkdir -p "${HOME}/.config/nvim"

echo "[+] linking files"

cp "config/aliases.sh" "${HOME}/.aliases"
# cp "config/zshrc" "${HOME}/.zshrc"

echo "[+] linking application configurations"

cp "config/1Password-agent.toml" "${HOME}/.config/1Password/ssh/agent.toml"
# cp "config/gitconfig" "${HOME}/.gitconfig"
cp "config/neovim-init.lua" "${HOME}/.config/nvim/init.lua"
cp "config/vscode-extensions.json" "${HOME}/.config/Code/User/extensions.json"
cp "config/vscode-settings.json" "${HOME}/.config/Code/User/settings.json"

echo "[+] installing basics"
sudo apt update
sudo apt install \
    build-essential \
    libssl-dev zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    curl \
    git \
    libncursesw5-dev \
    xz-utils \
    tk-dev \
    libxml2-dev \
    libxmlsec1-dev \
    libffi-dev \
    liblzma-dev

echo "[+] setting up pyenv"
rm -rf ${HOME}/.pyenv
curl -fsSL https://pyenv.run | bash
mkdir -p ${PYENV_ROOT}/plugins
rm -rf ${PYENV_ROOT}/plugins/pyenv-virtualenv
# figure out how to resolve pyenv-root without leaving the shell
git clone https://github.com/pyenv/pyenv-virtualenv.git ${PYENV_ROOT}/plugins/pyenv-virtualenv

echo "[+] setting up neovim"

sudo mkdir -p /opt
sudo chown -R $(whoami) /opt
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz
sudo rm -rf /opt/nvim
sudo tar -C /opt -xzf nvim-linux64.tar.gz
rm -rf nvim-linux64.tar.gz
mkdir -p bin
rm -rf bin/nvim
ln -s /opt/nvim-linux64/bin/nvim bin/nvim

# ...

echo "[*] configurations synced!"
