# INSTALLATION NOTES

Updated for reinstalling Linux in early 2019

**WARNING!** This has not yet been updated for using `pyenv` instead of `conda` for Python. Be warry of any Python-based installation steps.

The next time you wipe your hard drive or decide to upgrade and need to
start from scratch, this will help you out tremendously. Honestly, I'm
surprised you made it as far as you did without making one of these. Now
it's finally time to keep this stuff organized.

For Mac installs, you're mostly onlly going to care about the your
`bash_profile`, VSCode settings, and Anaconda/`python`. Since the Mac
installations are for your work computers, their setup will be very
different than your personal computer. In the near future, you should
separate out the Mac instructions into their own document to avoid this
one from bloating.

## Beginning Installation

Once your computer boots, you need to install the basics. These installs
are split between *applications* (e.g. Chrome, VSCode) and *utilities*
(e.g. `gcc`, `vim`). Before doing any installation, you'll want to make
sure that your current setup is up-to-date. You can do this either with
`apt update && apt upgrade` or by using the Update Manager.

### Utilities

These utilities are primarily command line tools and adjustments. Some
of these are actual tools and others are for your particular style.

1. Install command line basics:

   ```bash
   apt install build-essential git rsync tree unzip vim xclip
   ```

*NOTE:* `git` above is old, need to do some updates following https://git-scm.com/download/linux

1. Create a new SSH key:

   ```bash
   ssh-keygen -t ed25519 -C "mmoran0032@gmail.com"
   ```

1. Install `python` with Anaconda by following the instructions in
   `PYTHON.md` for your primary development work.

1. Run `python setup.sh` to link your bash profile, aliases, etc. to the
   required locations.

1. Install python utilities:

   ```bash
   # conda install -c conda-forge autopep8 doc8 glances
   ```

### Applications

The applications listed below are installed by downloading the
application from the respective website. These applications are your
default applications in the respective area.

- 1Password: 
- Firefox: <https://www.mozilla.org/en-US/firefox/new/>
- VSCode: <https://code.visualstudio.com/>

Additional steps for setting up VSCode are given in `EDITORS.md`.

Adjust any computer settings (Terminal background and scrollback,
sounds, user picture, etc) and download any files from the backed-up
external drive that you want.

## Yubikey

You've set up your two Yubikeys with Gitlab as SSH keys. If you're on a
new computer, you need to unload the keys from the Yubikey for it to
work with `git pull`. Run the following steps from `~/.ssh/`:

1. Pull IDs from key using `ssh-keygen -K`
1. Add identity to agent using `ssh-add -K`
1. Rename `*_rk` files to remove the `_rk` component.

You should have `id_ed25519_sk(.pub)` within `.ssh/`. Every interaction
with the git repo via SSH will now require the key and user to be
present.

I *think* the weirdness with renaming is due to how the keys were named
and stored when creating them. I created the keys following a
[Gitlab guide][3], with each key being created with

```sh
ssh-keygen -t ed25519-sk -O resident -f ~/.ssh/id_ed25519_sk
```

If I called the `_rk` or left that off, would it still work? Try it out
later, since figuring out getting both to work at the same time took
longer than you thought.

Good news is that you can do the following steps if you want these keys
to protect *any other SSH connection*:

- Unload the keys as above
- Provide the unloaded public key to the service

## Install History

Log of the installations you've done using this package. For
completeness, the partial Mac installs are also listed below, even
though they don't use the entirety of this repository and are not the
focus of tracking installations.

- 2024-06-11: *Material Security* Macbook
- 2023-02-19: `ARES-3` general updates
- 2021-06-07: *Duo Security* Macbook
- 2020-12-29: `ARES-3` (Linux Mint 19 desktop)
- 2020-08-22: `ARES-4m` (Linux Mint 20 laptop)
- 2020-07-19: `ARES-4` (Windows 10 + Windows Subsystem for Linux)
- 2019-02-19: `ARES-3` (Linux Mint 19 desktop)
- 2019-01-15: *White Ops* Macbook
- 2017-12-17: `ARES-2m` (Linux Mint 18 laptop)
- 2017-10-16: *Gartner* Macbook
- 2016-05-03: `ARES-2` (Ubuntu 16 laptop)

Previous installations and refreshes of Ubuntu and Linux Mint were not
recorded at the time, so the log is only complete for recent dates. For
instance, I had repeairs done on the `ARES-2*` laptop in July 2014, and
likely did a wipe-and-replace on the hard drive around that time.

[3]: https://about.gitlab.com/blog/2022/03/03/how-to-protect-gitlab-connected-ssh-key-with-yubikey/
